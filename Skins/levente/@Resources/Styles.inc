[TitleStyle]
X=160
Y=10
FontFace=微软雅黑
FontSize=16
Fontcolor=ffffff
FontEffectColor=7f7f7f
StringEffect=Border
AntiAlias=1

[InfoStyle]
X=160
Y=40
FontFace=微软雅黑
FontSize=10
Fontcolor=ffffff
FontEffectColor=7f7f7f
StringAlign=RIGHT
StringEffect=Border
AntiAlias=1

[UndercoverStyle]
X=0
Y=0
W=300
H=81
SolidColor=ffffff01
MouseOverAction=[!ShowMeter "Background"][!Redraw]
MouseLeaveAction=[!HideMeter "Background"][!Redraw]
; Disable right-click menu.
RightMouseUpAction=[]

[BackgroundStyle]
ImageName=#@#bg.png
X=0
Y=0
W=300
H=81
ScaleMargins=10,10,10,10
ImageAlpha=125
Hidden=1

[ChartBackgroundStyle]
ImageName=chart-bg.png
X=168
Y=10
W=122
H=61
ScaleMargins=10,10,10,10

[ChartLineStyle]
X=169
Y=11
W=120
H=60
GraphStart=LEFT
AntiAlias=1

[ChartHistogramStyle]
X=169
Y=10
W=120
H=60
GraphStart=LEFT
AntiAlias=1
